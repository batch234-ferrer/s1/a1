package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin // --> enable cross origin request via @CrossOrigin

public class PostController {

    @Autowired
    PostService postService;

    // Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);

    }

    // Activity 4 - Get all Posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(){

        Iterable<Post> allPosts = postService.getPost();
        return new ResponseEntity<>(allPosts, HttpStatus.OK);
    }

    /*
    // ---> Instructor's solution
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(){

        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }
    */

    // Update post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization")String stringToken, @RequestBody Post post){
        return postService.updatePost(postid, stringToken, post);
    }

    // Mini-capstone/Activity 5
    // Delete post by its ID
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization")String stringToken){

        postService.deletePost(postid, stringToken);
        return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
    }

    // Get myPosts
    @RequestMapping(value = "/posts/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> myPosts(@RequestHeader(value = "Authorization")String stringToken){

        Iterable<Post> myPosts = postService.myPosts(stringToken);
        return new ResponseEntity<>(myPosts,HttpStatus.OK);
    }


}
