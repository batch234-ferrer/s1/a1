package com.zuitt.wdc044.models;

import javax.persistence.*;
// this comes from the dependency that we have set up in pom.xml

@Entity
// this class is a representation of a database table via this annotation.
@Table(name = "posts")
// this annotation designates the table name.
public class Post {
    @Id
    // sets up the primary key
        @GeneratedValue
        // auto-increment
    private Long id;
    // id will be a long integer

    @Column
    // class properties
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    // indicates foreign key column
    private User user;

    // [Constructors]
        // default constructor for retrieval
    public Post(){}

        // parametrized
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    // [Getters and Setters]
    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){
        return user;
    }
    public void setUser(User user){
        this.user = user;
    }
}
