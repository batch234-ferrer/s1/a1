package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    // create a post
    void createPost(String stringToken, Post post);
    // retrieve a post
    Iterable<Post> getPost();
    //update post
    ResponseEntity updatePost(Long id, String stringToken, Post post);
    //delete post
    void deletePost(Long id, String stringToken);
    // get my posts
    Iterable<Post> myPosts(String stringToken);
}
