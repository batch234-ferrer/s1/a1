package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
// service provide, all business logic is here
public class UserServiceImpl implements UserService{
    @Autowired
    // marks the constructor, setter, properties, and methods from the object
    private UserRepository userRepository;

    public void createUser(User user){
        // creates user and save.
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        // check if username exists, otherwise, value is null.
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
