package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        // extraction of user/username from token to decipher which user is creating the post.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        // creation of new post
        Post newPost = new Post();

        // creation of the properties of that post
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        // author will come from the user extracted from the jwt
        newPost.setUser(author);

        // saving the post
        postRepository.save(newPost);
    }

    public Iterable<Post> getPost(){
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post){

        // (authenticatedUser) is logged-in user --> will match if he/she's the uploader(postAuthor) of the post(postForUpdating)
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post update successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }

    }
    public void deletePost(Long id, String stringToken){
        postRepository.deleteById(id);
    }

    public Iterable<Post> myPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return postRepository.findAll();
    }
}
