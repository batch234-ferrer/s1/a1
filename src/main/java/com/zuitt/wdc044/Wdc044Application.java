package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
// annotation --> with "@" sign, under the hood code to be used.
@RestController
// handles the endpoint for web requests.
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}
	//@GetMapping - tells Spring to use this method when a GET request is received.
	@GetMapping("/hello")
	//@RequestParam - annotation tells Spring to expect a name value in the request/url.
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){

		return String.format("Hello %s", name);
		// %s - any type but the return is a String
		// %c - character and the output is a unicode character
		// %b - any type but the output is boolean
	}

	// -----> Activity Solution <-----
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user){

		return String.format("Hi %s", user + "!");
	}

	// -----> Stretch Goal <-----
	@GetMapping("/nameage")

	public String nameage(@RequestParam(value = "user", defaultValue = "user") String user, @RequestParam(value = "age", defaultValue = "0") int age){

		return String.format("Hi %s", user + "!" + " Your age is " + age +".");
	}

}

/*
Command for running the application:
--> ./mvnw spring-boot:run
*/
// route for checking: localhost:8080/hello
// with parameter: localhost:8080/hello?parameter=value
